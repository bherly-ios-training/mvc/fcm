//
//  Network.swift
//  simpletodo-1
//
//  Created by Bherly Novrandy on 1/24/18.
//  Copyright © 2018 Bherly Novrandy. All rights reserved.
//

import Foundation

protocol FirebaseProtocol {
    func getFirebaseToken(success: () -> String, failure: () -> Error)
}

class Firebase: FirebaseProtocol {
    func getFirebaseToken(success: () -> String, failure: () -> Error) {
        
    }
}
